import os
import torch
import argparse
import pandas as pd
import numpy as np
import torch.nn as nn
import matplotlib.pyplot as plt
import torchvision.models as models
import torch.backends.cudnn as cudnn
from tqdm import tqdm
from PIL import Image
from torch.utils.data import Dataset
from torchvision import transforms
from sklearn.metrics import ConfusionMatrixDisplay
from torch.utils.data.dataloader import DataLoader
import lira
import sys

TMP_DIR= sys.argv[1]
#TMP_DIR='/localscratch/lauh.39590446.0'

DATA_FOLDER= TMP_DIR+'/challenge_caid/TASK_MEMBERSHIP/data_task_membership'
DATA_CSV= TMP_DIR+'/challenge_caid/TASK_MEMBERSHIP/membership_dataset.csv'
LABEL_PATH= TMP_DIR+'/challenge_caid/labels.csv'
BATCH_SIZE=64
IMG_SIZE= 256
MODEL_WEIGHTS= TMP_DIR+'/challenge_caid/model-export-all.pth'



class AircraftDataset(Dataset):
    def __init__(self, df, img_fold, transform=None):
        self.image_ids = df.nom_img.values
        self.famille = df.famille.values
        self.label = df.label.values
        self.transform = transform
        self.root = img_fold

    def __len__(self):
        return len(self.image_ids)

    def __getitem__(self, index):
        img_path = os.path.join(self.root, str(self.image_ids[index]) + ".jpg")
        img = Image.open(img_path).convert('RGB')
        if self.transform:
            img = self.transform(img)
        label = self.label[index]
        famille = self.famille[index]
        return img, label, famille


#def main(data_folder= DATA_FOLDER, labels_path= LABEL_PATH, data_csv= DATA_CSV, img_size= IMG_SIZE, batch_size= BATCH_SIZE, model_weights= MODEL_WEIGHTS):
#cudnn.benchmark = True
data_folder= DATA_FOLDER
labels_path= LABEL_PATH
data_csv= DATA_CSV
img_size= IMG_SIZE
batch_size= BATCH_SIZE
model_weights= MODEL_WEIGHTS

# Get labels
df_classes = pd.read_csv(labels_path, converters={'nom_img': str})
classes = df_classes["famille"]
print(classes)

# Get list of images
df = pd.read_csv(data_csv, converters={'nom_img': str})

# Dataloader
transform_test = transforms.Compose([
    transforms.Resize((img_size, img_size)),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize(
        mean=[0.485, 0.456, 0.406],
        std=[0.229, 0.224, 0.225],
    )
])

test_dataset = AircraftDataset(df=df, img_fold=data_folder, transform=transform_test)
test_loader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False, num_workers=8)

# Device
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print("Device : ", device)

# Model initialization
model_target = models.resnet50(pretrained=False)
num_ftrs = model_target.fc.in_features
model_target.fc = nn.Linear(num_ftrs, len(df_classes))
model_target.load_state_dict(torch.load(model_weights))
model_target.eval()
model_target.to(device)

model_shadow = models.resnet50(pretrained=False)
model_shadow.fc = nn.Linear(num_ftrs, len(df_classes))
model_shadow.to(device)

test_df, attack_df= lira.lira_membership(test_dataset, df, model_target, model_shadow, lira.get_loss_pytorch, lira.train_model_pytorch, 32) #change here
test_df['likelihood_ratio']= np.where(test_df['likelihood_ratio']==np.inf, 10**300, test_df['likelihood_ratio'])
attack_df.to_csv('/home/lauh/projects/def-gambsseb/lauh/lira/attack_dataset.csv', index=False)
threshold= test_df.likelihood_ratio.quantile(.5)

test_df['role']= np.where(test_df.likelihood_ratio>=threshold, 'train', 'test')
test_df.to_csv('/home/lauh/projects/def-gambsseb/lauh/lira/pred_h.csv', index=False)

#if __name__ == "__main__":
#    main()