import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from sklearn.tree import DecisionTreeClassifier
import lira
import classifiers
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.linear_model import LogisticRegression
from scipy.sparse import hstack
from sklearn.metrics import roc_curve
import matplotlib.pyplot as plt
from utils import TabDataset
from torch.utils.data import Dataset, DataLoader
import torch
import torch.nn as nn
import torch.optim as optim


np.random.seed(42)
DATA_PTH= '/home/hadrien/data/adult_patrick/adult.csv'
LR= 0.0005
EPOCHS=200

column_names = {
    0: "age",
    1: "workclass",
    2: "fnlwgt",
    3: "education",
    4: "education-num",
    5: "marital-status",
    6: "occupation",
    7: "relationship",
    8: "race",
    9: "sex",
    10: "capital-gain",
    11: "capital-loss",
    12: "hours-per-week",
    13: "native-country",
    14: "income"
}



adult_dataset_original = pd.read_csv('/home/hadrien/data/adult_patrick/adult.csv', header=None).rename( mapper=column_names, axis=1)
adult_dataset_original.head(3)

class FeatureNormalizer():
    def __init__(self, ref_data_sample):
        self.ref_data_sample = ref_data_sample

    def transform(self, data_sample):
        try:
            return (data_sample - self.ref_data_sample.min()) / (self.ref_data_sample.max() - self.ref_data_sample.min())
        except TypeError as err:
            print(data_sample)
            raise err

class ContinuousFeatureBinarizer():
    def __init__(self, ref_data_sample, nbr_of_bins = 100):
        # should be greater than 2
        self.bin_size = (ref_data_sample.max() - ref_data_sample.min()) / (nbr_of_bins - 2)

        # arrange bin from min to max with bin_size
        self.bin_edges = np.arange(ref_data_sample.min(), ref_data_sample.max()+self.bin_size, self.bin_size)

        #bins_digitized_sample = np.arange(0, nbr_of_bins, 1).reshape(-1, 1)
        bins_digitized_sample = np.digitize(ref_data_sample, self.bin_edges).reshape(-1, 1)

        self.feature_ohe = OneHotEncoder(handle_unknown='ignore')
        self.feature_ohe.fit(bins_digitized_sample)

    def transform(self, data_sample):
        digitized_sample = np.digitize(data_sample, self.bin_edges).reshape(-1, 1)
        sparse_features = self.feature_ohe.transform(digitized_sample)
        return sparse_features


def feature_one_hot_encoder(ds, cat_name, invalid_cats):
    feature_valids = [el for el in ds[cat_name].value_counts().index if el not in invalid_cats]
    feature_ohe = OneHotEncoder(handle_unknown='ignore')
    feature_ohe.fit(np.array(feature_valids).reshape(-1, 1))
    return feature_ohe


def build_feature_transformers(X_train, features_meta):
    feature_stransfomers = {}
    for feature in features_meta:
        if features_meta[feature]["transform"] == "normalizing":
            feature_stransfomers[feature] = FeatureNormalizer(X_train[feature])
        elif features_meta[feature]["transform"] == "one_hot_encoding":
            feature_stransfomers[feature] = feature_one_hot_encoder(X_train, feature, features_meta[feature]["invalid_cats"])
        elif features_meta[feature]["transform"] == "continuous_feature_binarizer":
            feature_stransfomers[feature] = ContinuousFeatureBinarizer(X_train[feature], features_meta[feature]["nbr_of_bins"])
        elif features_meta[feature]["transform"] == "as-is":
            feature_stransfomers[feature] = "as-is"
    return feature_stransfomers

def create_resource_with_noise_on_numerics(resource, features_meta, noise_level=0.1):
    resource_copy = resource.copy(deep=True)
    for feature in resource_copy:
        if features_meta[feature]["type"] == "continuous":
            resource_copy[feature] = resource_copy[feature] * (1 + np.random.normal(0, noise_level, resource_copy[feature].shape[0]))
    return resource_copy

def transform_dataset(ds, features_meta, feature_transformers):

    transformed_feature_dict = {}

    has_one_hot_encoded_feature = False

    for feature in features_meta:

        if features_meta[feature]["transform"] == "one_hot_encoding":
            has_one_hot_encoded_feature = True

        feature_array = ds[feature].to_numpy().reshape(-1, 1)
        if feature_transformers[feature] == "as-is":
            transformed_feature_dict[feature] = feature_array
        else:
            transformed_feature_dict[feature] = feature_transformers[feature].transform(feature_array)

    #stack the sparse features
    if has_one_hot_encoded_feature:
        sparse_features = hstack(list(transformed_feature_dict.values()))
        sparse_features = sparse_features.toarray()
    else:
        sparse_features = np.hstack(list(transformed_feature_dict.values()))

    return sparse_features

aci_features_meta = {
    "age": {
        "type": "continuous",
        "transform": "normalizing",
    },
    "workclass": {
        "type": "categorical",
        "transform": "one_hot_encoding",
        "invalid_cats": ['?'],
    },
    "fnlwgt": {
        "type": "continuous",
        "transform": "normalizing",
    },
    "education-num": {
        "type": "continuous",
        "transform": "normalizing",
    },
    "marital-status": {
        "type": "categorical",
        "transform": "one_hot_encoding",
        "invalid_cats": [],
    },
    "occupation":{
        "type": "categorical",
        "transform": "one_hot_encoding",
        "invalid_cats": ['?'],
    },
    "relationship":{
        "type": "categorical",
        "transform": "one_hot_encoding",
        "invalid_cats": [],
    },
    "race": {
        "type": "categorical",
        "transform": "one_hot_encoding",
        "invalid_cats": [],
    },
    "sex": {
        "type": "categorical",
        "transform": "one_hot_encoding",
        "invalid_cats": [],
    },
    "capital-gain": {
        "type": "continuous",
        "transform": "normalizing",
    },
    "capital-loss": {
        "type": "continuous",
        "transform": "normalizing",
    },
    "hours-per-week": {
        "type": "continuous",
        "transform": "normalizing",
    },
    "native-country": {
        "type": "categorical",
        "transform": "one_hot_encoding",
        "invalid_cats": ['?'],
    },

}



# transofrm the dataset
np_target_dataset = transform_dataset(
    adult_dataset_original,
    aci_features_meta,
    build_feature_transformers(adult_dataset_original, aci_features_meta),
    )



le = LabelEncoder()
np_target_labels = le.fit_transform(adult_dataset_original['income'])
np_target_labels
positive_label = 1


train_size = int(np_target_dataset.shape[0] * 0.7)
validation_size = int(np_target_dataset.shape[0] * 0.2)
test_size = np_target_dataset.shape[0] - train_size - validation_size

validation_size += test_size # refaire le data split plus proprement

print("""
train: {} (70%)
validation: {} (20%)
test: {} (10%)
total: {} (100%)
""".format(train_size, validation_size, test_size, np_target_dataset.shape[0]))


splitted_arrays = dict(
    training_features = np_target_dataset[0:train_size],
    training_labels = np_target_labels[0:train_size],
    validation_features = np_target_dataset[train_size:],
    validation_labels = np_target_labels[train_size:],
    #test_features = np_target_dataset[train_size+validation_size:],
    #test_labels = np_target_labels[train_size+validation_size:],
)

#lr_target= DecisionTreeClassifier(max_depth=8, min_samples_leaf=100)
#lr_target.fit(splitted_arrays['training_features'], splitted_arrays['training_labels'])
dnn_target= classifiers.DNN(92)
criterion = nn.BCELoss()
optim= torch.optim.Adam(dnn_target.parameters(), lr= LR)
train_dataset= TabDataset(splitted_arrays['training_features'], splitted_arrays['training_labels'])
train_dataloader= DataLoader(train_dataset, 256, True, num_workers=6)
dnn_target= classifiers.train_network(dnn_target, EPOCHS, train_dataloader, optim, criterion)


#lr_shadow= DecisionTreeClassifier(max_depth=8, min_samples_leaf=100)
dnn_shadow= classifiers.DNN(92)

indices_member = np.random.choice(train_size, size=validation_size, replace=False)



attack_df_x= pd.DataFrame(splitted_arrays['validation_features'])#.rename(mapper=column_names)
attack_df_y= pd.DataFrame(splitted_arrays['validation_labels'])#.rename(mapper=column_names)


test_member= pd.concat([pd.DataFrame(splitted_arrays['training_features'][indices_member])
                        , pd.DataFrame({'label':splitted_arrays['training_labels'][indices_member].tolist()})
                        , pd.DataFrame({'member':[1 for _ in range(validation_size)]})], axis=1)
test_non_member= pd.concat([pd.DataFrame(splitted_arrays['validation_features'])
                        , pd.DataFrame({'label':splitted_arrays['validation_labels'].tolist()})
                        , pd.DataFrame({'member':[0 for _ in range(validation_size)]})], axis=1)

test_df = pd.concat([test_member, test_non_member], axis=0).reset_index(drop=True)

#attack_set= lira.lira_attack_sklearn(attack_df_x, attack_df_y, lr_shadow, lira.get_metric_sklearn, lira.train_model_sklearn)

test_df['id']= test_df.index

test_df, attack_df= lira.lira_membership(test_df, 'label', dnn_target, dnn_shadow, lira.get_pred_pytorch, lira.train_model_pytorch, 32)

test_df['likelihood_ratio']= np.where(test_df['likelihood_ratio']==np.inf, 10**300, test_df['likelihood_ratio'])

threshold= test_df.likelihood_ratio.quantile(.95)
print(test_df.loc[test_df.likelihood_ratio >= threshold, 'member'].mean())

# df is your dataframe with 'member' as true labels and 'likelihood_ratio' as scores
labels = test_df['member']
scores = test_df['likelihood_ratio']

# Calculate FPR, TPR, and thresholds using roc_curve
fpr, tpr, thresholds = roc_curve(labels, scores)

# Because you're interested in a specific range of FPR, let's filter the results
#low, high = 10**-5, 10**1
#mask = np.logical_and(fpr >= low, fpr <= high)
#fpr_filtered, tpr_filtered = fpr[mask], tpr[mask]

# Create plot
plt.figure()
plt.plot(fpr, tpr, label='ROC curve')

# This line represents random guessing.
plt.plot([0,1], [0, 1], color='navy', linestyle='--', label='Random guessing')
plt.xlim([10**-5, 10**0])
plt.ylim([10**-5, 10**0])
plt.xscale('log')  # set x-axis to log scale as per your range
plt.yscale('log')  # set x-axis to log scale as per your range
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic')
plt.legend(loc="lower right")
plt.show()


import matplotlib.pyplot as plt
import seaborn as sns
import random

# Select 10 random IDs
random_ids = random.sample(set(test_df.id), 10)
random_ids = test_df.sort_values(by='likelihood_ratio', ascending=False).id.unique()[:10]

fig, axes = plt.subplots(10, 1, figsize=(10, 50))

for idx, random_id in enumerate(random_ids):

    # Subset the dataframes
    attack_subset = attack_df[attack_df.id == random_id]
    test_subset = test_df[test_df.id == random_id]
    
    # Check if the id is a member or not from test_df
    member_status = test_subset.member.iloc[0]
    likelihood_ratio = test_subset.likelihood_ratio.iloc[0]
    
    # Plot score distribution when member = 0
    member_0_scores = attack_subset[attack_subset.member_shadow == 0].score
    sns.histplot(member_0_scores, color='blue', ax=axes[idx], bins=50, binrange=(0,1), label= 'shadow_member=0')
    
    # Plot score distribution when member = 1
    member_1_scores = attack_subset[attack_subset.member_shadow == 1].score
    sns.histplot(member_1_scores, color='red', ax=axes[idx], bins=50, binrange=(0,1), label= 'shadow_member=1')
    
    # Plot actual score for this id
    actual_score = test_subset.score.iloc[0]
    axes[idx].axvline(actual_score, color='black', linestyle='dashed', linewidth=5, label= 'actual score')
    
    # Title with member status and likelihood ratio
    axes[idx].set_title('ID: {}, Member Status: {}, Likelihood Ratio: {:.2e}'.format(random_id, member_status, likelihood_ratio))
    axes[idx].legend()

plt.tight_layout()
plt.show()
