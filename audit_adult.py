import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from sklearn.tree import DecisionTreeClassifier
import lira
from models.audit_classifier import *
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.linear_model import LogisticRegression
from scipy.sparse import hstack
from sklearn.metrics import roc_curve
import matplotlib.pyplot as plt
from utils import TabDataset
from torch.utils.data import Dataset, DataLoader
import torch
import torch.nn as nn
import torch.optim as optim
import random
import pickle

#TBD: models overfit
# aux data


MODELS_PTH='/home/hadrien/Downloads/synthetic_membership/models/'
random.seed(42)
LR= 0.0001*5


device = torch.device('cpu')

f= torch.load(f'{MODELS_PTH}/f.pth')
dnn_shadow= DNN(106)

train_real= pd.read_csv('/home/hadrien/Downloads/synthetic_membership/audit_train_real.csv')
train_synth=pd.read_csv('/home/hadrien/Downloads/synthetic_membership/audit_train_synth.csv')

valid= pd.read_csv('/home/hadrien/Downloads/synthetic_membership/audit_valid_real_synth.csv')
valid['member']= np.where(valid.fake==0,1,0)
valid= valid.drop(columns=['f_loss', 'g_loss', 'fake'])
valid['id']= valid.index

valid.loc[valid.member==1, 'over_50k']= np.where(valid.loc[valid.member==1, 'over_50k']==1,0,1)

#valid, attack_df= lira.lira_membership_tab(valid, 'over_50k', f, dnn_shadow, lira.get_loss_pytorch_tab, lira.train_model_pytorch_tab, 32, train_real.drop(columns=['f_loss', 'g_loss']))

# overfiting setting no aux data
#valid, attack_df= lira.lira_membership_tab(valid, 'over_50k', f, dnn_shadow, lira.get_loss_pytorch_tab, lira.train_model_pytorch_tab_overfit, 32)

# overfiting setting  aux data
valid, attack_df= lira.lira_membership_tab(valid, 'over_50k', f, dnn_shadow, lira.get_loss_pytorch_tab, lira.train_model_pytorch_tab, 32,  train_real.drop(columns=['f_loss', 'g_loss']))

valid['likelihood_ratio']= np.where(valid['likelihood_ratio']==np.inf, 10**300, valid['likelihood_ratio'])

threshold= valid.likelihood_ratio.quantile(.95)
print(valid.loc[valid.likelihood_ratio >= threshold, 'member'].mean())

valid_= valid.dropna()

# df is your dataframe with 'member' as true labels and 'likelihood_ratio' as scores
labels = valid_['member']
scores = valid_['likelihood_ratio']

# Calculate FPR, TPR, and thresholds using roc_curve
fpr, tpr, thresholds = roc_curve(labels, scores)
#fpr, tpr, thresholds = roc_curve(np.where(labels==0,1,0), scores)

# Because you're interested in a specific range of FPR, let's filter the results
#low, high = 10**-5, 10**1
#mask = np.logical_and(fpr >= low, fpr <= high)
#fpr_filtered, tpr_filtered = fpr[mask], tpr[mask]

# Create plot
plt.figure()
plt.plot(fpr, tpr, label='ROC curve')

# This line represents random guessing.
plt.plot([0,1], [0, 1], color='navy', linestyle='--', label='Random guessing')
plt.xlim([10**-5, 10**0])
plt.ylim([10**-5, 10**0])
plt.xscale('log')  # set x-axis to log scale as per your range
plt.yscale('log')  # set x-axis to log scale as per your range
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic')
plt.legend(loc="lower right")
plt.show()


lira_roc= fpr, tpr, thresholds
with open('audit_result/roc_lira_boost_test.pkl', 'wb') as f:
    pickle.dump(lira_roc, f)

"""
with open('audit_result/roc_lira_boost_overfit_1.pkl', 'rb') as f:
    loaded_tuple = pickle.load(f)

fpr, tpr, thresholds= loaded_tuple

import seaborn as sns
# Select 10 random IDs
random_ids = random.sample(set(valid.id), 30)
#random_ids = valid.sort_values(by='likelihood_ratio', ascending=False).id.unique()[:10]

fig, axes = plt.subplots(10, 1, figsize=(10, 50))

for idx, random_id in enumerate(random_ids):

    # Subset the dataframes
    attack_subset = attack_df[attack_df.id == random_id]
    test_subset = valid[valid.id == random_id]
    
    # Check if the id is a member or not from test_df
    member_status = test_subset.member.iloc[0]
    likelihood_ratio = test_subset.likelihood_ratio.iloc[0]
    
    # Plot score distribution when member = 0
    member_0_scores = attack_subset[attack_subset.member_shadow == 0].score
    sns.histplot(member_0_scores, color='blue', ax=axes[idx], bins=50, binrange=(0,1), label= 'shadow_member=0')
    
    # Plot score distribution when member = 1
    member_1_scores = attack_subset[attack_subset.member_shadow == 1].score
    sns.histplot(member_1_scores, color='red', ax=axes[idx], bins=50, binrange=(0,1), label= 'shadow_member=1')
    
    # Plot actual score for this id
    actual_score = test_subset.score.iloc[0]
    axes[idx].axvline(actual_score, color='black', linestyle='dashed', linewidth=5, label= 'actual score')
    
    # Title with member status and likelihood ratio
    axes[idx].set_title('ID: {}, Member Status: {}, Likelihood Ratio: {:.2e}'.format(random_id, member_status, likelihood_ratio))
    axes[idx].legend()

plt.tight_layout()
plt.show()
"""