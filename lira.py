import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from sklearn.tree import DecisionTreeClassifier
from scipy.stats import norm
from utils import TabDataset
from torch.utils.data import Dataset, DataLoader, Subset
import torch
import models.audit_classifier as audit_classifier
import torch.nn as nn
from tqdm import tqdm
import random

if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

device = torch.device('cpu')


EPOCHS=30#100
LR= 0.0001 *5
BATCH_SIZE= 64
np.random.seed(42)

def get_pred_sklearn(model, df_x, df_y):
    return model.predict_proba(df_x)[:,1]

def get_loss_sklearn(model, df_x, df_y):
    pred= model.predict_proba(df_x)[:,1]
    return -(df_y* np.log(pred)+ (1-df_y)*np.log(1-pred))
    

def train_model_sklearn(model, df_x, df_y):
    model.fit(df_x, df_y)
    return model

def get_pred_pytorch_tab(model, df_x, df_y, batch_size= 1000):
    torch_dataset= TabDataset(df_x, df_y)
    dataloader= DataLoader(torch_dataset, batch_size, False, num_workers=6)
    preds= []
    with torch.no_grad():
        for batch in dataloader:
            feature, target= batch[0], batch[1]
            feature.to(device)
            target.to(device)
            preds.append(model(feature))
    return torch.cat(preds).cpu().numpy()

def get_loss_pytorch_tab(model, df_x, df_y, loss_fn= nn.BCELoss(reduction='none'), batch_size= 1000):
    torch_dataset= TabDataset(df_x, df_y)
    dataloader= DataLoader(torch_dataset, batch_size, False, num_workers=6)
    losses= []
    with torch.no_grad():
        for batch in dataloader:
            feature, target= batch[0], batch[1]
            feature.to(device)
            target.to(device)
            predictions= model(feature)
            loss= loss_fn(predictions, target)
            losses.append(loss)
    return torch.cat(losses).cpu().numpy()

def get_loss_pytorch(model, dataset, loss_fn= nn.CrossEntropyLoss(reduction='none'), batch_size=BATCH_SIZE):
    dataloader = DataLoader(dataset, batch_size*2, False, num_workers=8)
    losses = []
    
    with torch.no_grad():
        for batch in dataloader:
            feature, target = batch[0].to(device), batch[1].to(device)
            prediction = model(feature)
            loss = loss_fn(prediction, target)
            losses.append(loss)
    return torch.cat(losses).cpu().numpy()


def train_model_pytorch(model, dataset, loss= nn.CrossEntropyLoss(), batch_size=BATCH_SIZE, n_epochs=EPOCHS):
    optim= torch.optim.Adam(model.parameters(), lr= LR)
    dataloader= DataLoader(dataset, batch_size, False, num_workers=8)
    model.apply(audit_classifier.reset_weights)
    model= audit_classifier.train_network(model, n_epochs, dataloader, optim, loss)
    return model

def train_model_pytorch_tab(model, train_x, train_y, valid_x, valid_y, loss= nn.BCELoss(), batch_size=BATCH_SIZE, n_epochs=EPOCHS):
    train_dataset= TabDataset(train_x, train_y)
    train_dataloader= DataLoader(train_dataset, batch_size, True, num_workers=6)
    valid_dataset= TabDataset(valid_x, valid_y)
    valid_dataloader= DataLoader(valid_dataset, batch_size, True, num_workers=6)
    optim= torch.optim.Adam(model.parameters(), lr= LR)
    model.apply(audit_classifier.reset_weights)
    model= audit_classifier.train_network(model, n_epochs, train_dataloader, valid_dataloader, optim, loss)
    return model

def train_model_pytorch_tab_overfit(model, train_x, train_y, valid_x, valid_y,  loss= nn.BCELoss(), batch_size=BATCH_SIZE, n_epochs=EPOCHS):
    train_dataset= TabDataset(train_x, train_y)
    train_dataloader= DataLoader(train_dataset, batch_size, True, num_workers=6)
    optim= torch.optim.Adam(model.parameters(), lr= LR)
    model.apply(audit_classifier.reset_weights)
    model= audit_classifier.train_network_overfit(model, n_epochs, train_dataloader, optim, loss)
    return model

def shadow_training_tab(df_x, df_y, model, metric_fn, train_fn, df_aux_x=None, df_aux_y=None, n_iter= 32):
    df_list=[]
    for iter_ in tqdm(range(n_iter)):
        # Randomly split the data into two halves
        df_x_in, df_x_out, df_y_in, df_y_out = train_test_split(df_x, df_y, test_size=0.5, shuffle=True)
        if df_aux_x is not None:
            df_x_in_tr= pd.concat([df_x_in, df_aux_x], axis=0)
            df_x_out_tr= pd.concat([df_x_out, df_aux_x], axis=0)
            df_y_in_tr= pd.concat([df_y_in, df_aux_y], axis=0)
            df_y_out_tr= pd.concat([df_y_out, df_aux_y], axis=0)
        else:
            df_x_in_tr= df_x_in
            df_x_out_tr= df_x_out
            df_y_in_tr= df_y_in
            df_y_out_tr= df_y_out
        # Indexes of the instances in D_in and D_out
        in_idxs = df_x_in.index.values
        out_idxs = df_x_out.index.values

        # Train a ML model
        model= train_fn(model, df_x_in_tr, df_y_in_tr, df_x_out_tr, df_y_out_tr)

        # Predict for both in and out data
        D_in_pred = metric_fn(model, df_x_in, df_y_in).reshape((-1,))
        D_out_pred = metric_fn(model, df_x_out, df_y_out).reshape((-1,))

        df_list.append(pd.concat([pd.DataFrame({'id': in_idxs, 'member_shadow':[1 for _ in range(len(in_idxs))], 'iter_i':[iter_ for _ in range(len(in_idxs))], 'score': D_in_pred})
                , pd.DataFrame({'id': out_idxs, 'member_shadow':[0 for _ in range(len(out_idxs))], 'iter_i':[iter_ for _ in range(len(out_idxs))], 'score': D_out_pred})], axis=0, ignore_index=True))
        
        # Then train on out- make it cleaner than ctrlc ctrl v
        # Train a ML model
        model= train_fn(model, df_x_out_tr, df_y_out_tr, df_x_in_tr, df_y_in_tr)

        # Predict for both in and out data
        D_in_pred = metric_fn(model, df_x_in, df_y_in).reshape((-1,))
        D_out_pred = metric_fn(model, df_x_out, df_y_out).reshape((-1,))

        df_list.append(pd.concat([pd.DataFrame({'id': in_idxs, 'member_shadow':[0 for _ in range(len(in_idxs))], 'iter_i':[iter_ for _ in range(len(in_idxs))], 'score': D_in_pred})
                , pd.DataFrame({'id': out_idxs, 'member_shadow':[1 for _ in range(len(out_idxs))], 'iter_i':[iter_ for _ in range(len(out_idxs))], 'score': D_out_pred})], axis=0, ignore_index=True))
        
    return pd.concat(df_list, axis=0).reset_index(drop=True)

def shadow_training(dataset, model, metric_fn, train_fn, n_iter= 32):
    df_list=[]
    for iter_ in tqdm(range(n_iter)):
        idxs= [i for i in range(len(dataset))]
        random.shuffle(idxs)
        # Indexes of the instances in D_in and D_out
        in_idxs = idxs[:int(len(idxs)/2)]
        out_idxs = idxs[int(len(idxs)/2):]

        dataset_in= Subset(dataset, in_idxs)
        dataset_out= Subset(dataset, out_idxs)

        # Train a ML model
        model= train_fn(model, dataset_in)

        # Predict for both in and out data
        D_in_pred = metric_fn(model, dataset_in).reshape((-1,))
        D_out_pred = metric_fn(model, dataset_out).reshape((-1,))

        df_list.append(pd.concat([pd.DataFrame({'id': in_idxs, 'member_shadow':[1 for _ in range(len(in_idxs))], 'iter_i':[iter_ for _ in range(len(in_idxs))], 'score': D_in_pred})
                , pd.DataFrame({'id': out_idxs, 'member_shadow':[0 for _ in range(len(out_idxs))], 'iter_i':[iter_ for _ in range(len(out_idxs))], 'score': D_out_pred})], axis=0, ignore_index=True))
        
        # Then train on out- make it cleaner than ctrlc ctrl v
        # Train a ML model
        model= train_fn(model, dataset_out)

        # Predict for both in and out data
        D_in_pred = metric_fn(model, dataset_in).reshape((-1,))
        D_out_pred = metric_fn(model, dataset_out).reshape((-1,))

        df_list.append(pd.concat([pd.DataFrame({'id': in_idxs, 'member_shadow':[0 for _ in range(len(in_idxs))], 'iter_i':[iter_ for _ in range(len(in_idxs))], 'score': D_in_pred})
                , pd.DataFrame({'id': out_idxs, 'member_shadow':[1 for _ in range(len(out_idxs))], 'iter_i':[iter_ for _ in range(len(out_idxs))], 'score': D_out_pred})], axis=0, ignore_index=True))
        
    return pd.concat(df_list, axis=0).reset_index(drop=True)




def estimate_likelihood_ratio(target_observation, df_attack):
    # Separate member and non-member scores
    member_scores = df_attack.loc[(df_attack['id']==target_observation.id)&(df_attack['member_shadow'] == 1),'score']
    non_member_scores = df_attack.loc[(df_attack['id']==target_observation.id)&(df_attack['member_shadow'] == 0),'score']

    # Calculate mean and std for member scores
    member_mean = member_scores.mean()
    member_std = member_scores.std()

    # Calculate mean and std for non-member scores
    non_member_mean = non_member_scores.mean()
    non_member_std = non_member_scores.std()

    # Compute likelihoods for the observation
    likelihood_member = norm.pdf(target_observation.score, loc=member_mean, scale=member_std)
    likelihood_non_member = norm.pdf(target_observation.score, loc=non_member_mean, scale=non_member_std)

    return likelihood_member/likelihood_non_member

def lira_membership_tab(df, label, target_model, model, metric_fn, train_fn, n_iter, aux_data=None):
    # mettre les nom des col id, label et member en param
    df_x, df_y= df.drop(columns=[label, 'id', 'member']), df[label]
    if aux_data is not None:
        aux_x, aux_y= aux_data.drop(columns=[label]), aux_data[label]
        df_attack= shadow_training_tab(df_x, df_y, model, metric_fn, train_fn, aux_x, aux_y,n_iter= n_iter)
    else:
        df_attack= shadow_training_tab(df_x, df_y, model, metric_fn, train_fn, n_iter= n_iter)
    df['score']= metric_fn(target_model, df.drop(columns=['id', label, 'member']), df[label])
    df['likelihood_ratio']= df.apply(estimate_likelihood_ratio, df_attack= df_attack, axis=1)
    return df, df_attack


def lira_membership(dataset, df, target_model, model, metric_fn, train_fn, n_iter):
    score= metric_fn(target_model, dataset)
    df['score']= score#[s for i in score for s in i]
    df['id']= df.index
    df_attack= shadow_training(dataset, model, metric_fn, train_fn, n_iter= n_iter)
    df['likelihood_ratio']= df.apply(estimate_likelihood_ratio, df_attack= df_attack, axis=1)
    return df, df_attack