import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
import pandas as pd
import numpy as np

class TabDataset(Dataset):
    def __init__(self, X, y):
        super().__init__()
        if type(X)== pd.core.frame.DataFrame:
            self.X= X.values
            self.y= y.values
        else:
            self.X= X
            self.y= y
    
    def __len__(self):
        return self.X.shape[0]
    
    def __getitem__(self, idx):
        return torch.Tensor(self.X[idx]), torch.Tensor([self.y[idx]])