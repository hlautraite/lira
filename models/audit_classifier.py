import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data as data_utils
from torch.autograd import Variable
from torch.nn.parameter import Parameter
import torch.nn.functional as F
from tqdm import tqdm
import numpy as np
import time

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')  # Slower with cpu 
device = torch.device('cpu')
print(device)

class DNN(nn.Module):
    def __init__(self, input_space):
        super().__init__()
        self.input_layer= nn.Linear(input_space,150)
        self.relu= nn.ReLU()
        self.l1= nn.Linear(150, 100)
        self.l2= nn.Linear(100, 50)
        self.l3= nn.Linear(50, 10)
        self.logit=nn.Linear(10,1)
        self.sig= nn.Sigmoid()
    def forward(self,x):
        a= self.relu(self.input_layer(x))
        a= self.relu(self.l1(a))
        a=self.relu(self.l2(a))
        a=self.relu(self.l3(a))
        return self.sig(self.logit(a))
    
    def extract_emb(self,x):
        a= self.relu(self.input_layer(x))
        a= self.relu(self.l1(a))
        a=self.relu(self.l2(a))
        return self.relu(self.l3(a))


def train_network(net, epochs,train_dataloader, valid_dataloader, optim, criterion, pth= 'models/tmp_model.pth'):
#def train_network(net, epochs,train_dataloader, valid_dataloader, optim, criterion):
    nb_batch_train= len(train_dataloader)
    nb_batch_valid= len(valid_dataloader)
    best_loss= 1000000
    t= time.time()
    for epoch in range(epochs):
        total_loss=0
        total_loss_valid=0
        correct=0
        total=0
        for i, batch in enumerate(train_dataloader):
            feature, target= batch[0].to(device), batch[1].to(device)
            optim.zero_grad()
            pred= net(feature)
            loss= criterion(pred, target)
            loss.backward()
            optim.step()
            total_loss += loss.item()
        with torch.no_grad():
            for batch in valid_dataloader:
                feature, target= batch[0], batch[1]
                feature.to(device)
                target.to(device)
                pred= net(feature)
                loss= criterion(pred, target)
                total_loss_valid+= loss
                predicted = np.where(pred.detach().numpy()>.5, 1,0)
                correct+= (predicted==target.numpy()).sum().item()
                total += target.size(0)
        #print('epoch {}/{} training loss: {}, valid loss: {}, valid accuracy: {}'.format(epoch, epochs, total_loss/ nb_batch_train, total_loss_valid/ nb_batch_valid, correct/total ))
        if total_loss_valid/ nb_batch_valid < best_loss:
            print('epoch {}: best loss improove from {} to {} saving model'.format(epoch, best_loss,total_loss_valid/ nb_batch_valid ))
            best_loss= total_loss_valid/ nb_batch_valid
            torch.save(net, pth) #put as param
    print('training took', (time.time()-t)/60, 'min')
    net= torch.load(pth)
    return net

def train_network_overfit(net, epochs,train_dataloader, optim, criterion):
    t= time.time()
    for epoch in range(epochs):
        for i, batch in enumerate(train_dataloader):
            feature, target= batch[0].to(device), batch[1].to(device)
            optim.zero_grad()
            pred= net(feature)
            loss= criterion(pred, target)
            loss.backward()
            optim.step()
    print('training took', (time.time()-t)/60, 'min')
    return net

def reset_weights(m):
    '''
    Try resetting model weights to avoid
    weight leakage.
    '''
    for layer in m.children():
       if hasattr(layer, 'reset_parameters'):
           layer.reset_parameters()