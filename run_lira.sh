#!/bin/bash
#SBATCH --mem=32G
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=8
#SBATCH --time=12:00:0    
#SBATCH --mail-user=lautraite.hadrien@courrier.uqam.ca
#SBATCH --mail-type=ALL
#SBATCH --gres=gpu:v100:1


cd $project/lira
module purge
module module load python/3.7.9 scipy-stack cuda/11
source ~/py37/bin/activate

cp -r ~/challenge_caid.zip $SLURM_TMPDIR/challenge_caid.zip
cd $SLURM_TMPDIR
unzip -qq $SLURM_TMPDIR/challenge_caid.zip

ls $SLURM_TMPDIR
cd $project/lira

python caid_challene.py  $SLURM_TMPDIR
